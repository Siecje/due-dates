from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, logout, login, get_user_model
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy, reverse
from django.views import generic
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render
from .forms import RegisterForm, ResetPasswordForm, ForgotUsernameForm, \
                   EditUserForm, ChangePasswordForm, LoginForm


class RegisterView(generic.FormView):
    template_name = 'accounts/register.html'
    form_class = RegisterForm
    success_url = reverse_lazy('dashboard')

    def form_valid(self, form):
        form.save(self.request)
        return super(RegisterView, self).form_valid(form)

'''
class RegisterView(generic.FormView):
    template_name = 'accounts/register.html'
    form_class = UserCreationForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        user = form.save()
        #would be nice to have login in the view
        # and not in the form
        # as you can use the form to make users
        # I think the password is wrong
        user1 = authenticate(
            username=user.username,
            password=user.password
        )
        login(self.request, user1)
        return super(RegisterView, self).form_valid(form)
'''

def login_user(request):
    next = redirect_to = request.POST.get('next',
                                   request.GET.get('next', None))
    form = LoginForm()
    error_message = ''
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            # get user from database for username to be correct capitalization
            try:
                user = get_user_model().objects.get(username__iexact=username)
            except get_user_model().DoesNotExist:
                error_message = "Username Does Not Exist."
                return render(request, 'accounts/login.html',
                    {'form': form, 'error_message': error_message})
            user = authenticate(username=user.username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    if next is not None:
                        return HttpResponseRedirect(next)
                    else:
                        return HttpResponseRedirect(
                            reverse(settings.LOGIN_REDIRECT_URL))
                else:
                    # Account is disabled
                    error_message = "Your account has been disabled."
            else:
                error_message = "Incorrect password for this username"

    return render(request, 'accounts/login.html',
        {'form': form, 'error_message': error_message})


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))


def reset_password(request):
    form = ResetPasswordForm()
    if request.method == 'POST':
        form = ResetPasswordForm(request.POST)
        if form.is_valid():
            # Check if there is an email associated with the user
            user = get_user_model().objects.get(
                username__iexact=form.cleaned_data['username'])
            if user.email:
                messages.success(request, "Email sent.")
            else:
                messages.error(
                    request, "There is no email linked with this account.")
            return HttpResponseRedirect(reverse('reset_password'))
    return render(request, 'accounts/reset_password.html', {'form': form})


def forgot_username(request):
    form = ForgotUsernameForm()
    if request.method == 'POST':
        form = ForgotUsernameForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            try:
                get_user_model()._default_manager.get(email__iexact=email)
                messages.success(request, "Email sent.")
            except get_user_model().DoesNotExist:
                messages.error(request, "Email sent.")
            return HttpResponseRedirect(reverse('forgot_username'))
    return render(request, 'accounts/forgot_username.html', {'form': form})


@login_required(login_url=reverse_lazy('login'))
def account(request):
    form = EditUserForm(instance=request.user)
    password_form = ChangePasswordForm(instance=request.user)
    if request.method == 'POST':
        if 'username' in request.POST:
            form = EditUserForm(request.POST, instance=request.user)
            if form.is_valid():
                form.save()
                messages.success(request, "Account Updated.")
                return HttpResponseRedirect(reverse('account'))
        elif 'password' in request.POST:
            password_form = ChangePasswordForm(
                request.POST, instance=request.user)
            if password_form.is_valid():
                password_form.save()
                messages.success(request, 'Password Changed.')
                return HttpResponseRedirect(reverse('account'))
            else:
                for field, errors in password_form.errors.items():
                    for error in errors:
                        messages.error(request, error)
    return render(request, 'accounts/account.html',
        {'form': form, 'password_form': password_form})
