from django.conf.urls import patterns, url
from . import views


urlpatterns = patterns('',
    url(r'^register/$', views.RegisterView.as_view(), name='register'),
    url(r'^login/$', views.login_user, name='login'),
    url(r'^logout/$', views.logout_user, name='logout'),
    url(r'^reset-password/$',
        views.reset_password, name='reset_password'),
    url(r'^forgot-username/$',
        views.forgot_username, name='forgot_username'),
    url(r'^account/$', views.account, name='account'),

    # password reset
    url(r'^user/password/reset/$',
        'django.contrib.auth.views.password_reset',
        {'post_reset_redirect': '/user/password/reset/done/'},
        name="password_reset"),
    (r'^user/password/reset/done/$',
        'django.contrib.auth.views.password_reset_done'),
    (r'^user/password/reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
        'django.contrib.auth.views.password_reset_confirm',
        {'post_reset_redirect': '/user/password/done/'}),
    (r'^user/password/done/$',
        'django.contrib.auth.views.password_reset_complete'),
)
