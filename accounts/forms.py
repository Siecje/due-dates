from django.contrib.auth import authenticate, login, get_user_model
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import ugettext, ugettext_lazy as _


class RegisterForm(UserCreationForm):

    class Meta:
        model = get_user_model()
        fields = ('username', 'email', 'password1', 'password2')
        help_texts = {
            'email': "Only used for account recovery.",
        }
        #TODO: why doesn't this work?
        labels = {
            'password2': "Password Confirmation",
        }

    def clean_username(self):
        # ensure that username is incase sensitive and unique
        username = self.cleaned_data["username"]
        try:
            get_user_model()._default_manager.get(username__iexact=username)
        except get_user_model().DoesNotExist:
            return username
        raise forms.ValidationError(
            #TODO: change message to 'Username taken'
            self.error_messages['duplicate_username'],
            code='duplicate_username',
        )

    def save(self, request=None, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        email = self.cleaned_data['email']
        if email != '':
            user.email = email
        if commit is True:
            user.save()
        # if request is specified then login
        if commit is True and request is not None:
            user = authenticate(
                username=self.cleaned_data["username"],
                password=self.cleaned_data["password1"]
            )
            login(request, user)
        return user


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class ResetPasswordForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['username']
        help_texts = {
            'username': 'Enter your username to send an email to the email associated with the account.',
        }


class ForgotUsernameForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['email']
        helptexts = {
            'email': 'Enter your email to recieve a list of usernames linked with the email address.',
        }


class EditUserForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['username', 'email']


class ChangePasswordForm(forms.ModelForm):
    new_password = forms.CharField(widget=forms.PasswordInput)
    confirm_new_password = forms.CharField(widget=forms.PasswordInput)

    error_messages = {
        'password_mismatch': _("The two new password fields didn't match."),
    }

    class Meta:
        model = get_user_model()
        fields = ['password']
        labels = {
            'password': 'Current Password',
        }
        widgets = {
            'password': forms.PasswordInput,
        }

    def clean_password(self):
        password = self.cleaned_data.get('password')
        if not self.instance.check_password(password):
            raise forms.ValidationError('Current Password is Invalid.')
        return password

    def clean_confirm_new_password(self):
        password1 = self.cleaned_data.get("new_password")
        password2 = self.cleaned_data.get("confirm_new_password")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        self.instance.set_password(
            self.cleaned_data.get("confirm_new_password"))
        if commit:
            self.instance.save()
        return self.instance
