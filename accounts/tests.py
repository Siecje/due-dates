from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.client import Client
from django.test.utils import setup_test_environment
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser


class AccountTestSuite(TestCase):

    def test_registration_page_info(self):
        """
        Ensure the registration page has the correct information
        """
        response = self.client.get(reverse('register'))
        self.assertContains(response, 'Register')
        self.assertContains(response, 'Username')
        self.assertContains(response, 'Email')
        self.assertContains(response, 'Only used for account recovery')
        self.assertContains(response, 'Password')
        self.assertContains(response, 'Password Confirmation')

    def test_registration_form(self):
        """
        Test that posting to the registration page will create a new user
        and that user will be logged in
        """
        # ensure the user does not exist
        self.assertRaises(get_user_model().DoesNotExist,
            get_user_model().objects.get,
            username='NewUser')
        c = Client()
        response = c.post(reverse('register'),
            {u'username': [u'NewUser'], u'password1': [u'test'],
                u'email': [u''], u'password2': [u'test']})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'],
            'http://testserver' + reverse('dashboard'))
        try:
            user = get_user_model().objects.get(username='NewUser')
        except get_user_model().DoesNotExist:
            self.assertTrue(False)
        response = c.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_authenticated())

    def test_registration_already_exists(self):
        """
        Test that registering with a username that already exists
        displays the proper error message
        usernames should be incase sensitive
        """
        self.assertRaises(
            get_user_model().DoesNotExist,
            get_user_model().objects.get,
            username='NewUser')
        c = Client()
        response = c.post(reverse('register'),
            {u'username': [u'NewUser'], u'password1': [u'test'],
                u'email': [u''], u'password2': [u'test']}, follow=True)
        self.assertTrue(isinstance(response.context['user'], get_user_model()))
        try:
            user = get_user_model().objects.get(username='NewUser')
        except get_user_model().DoesNotExist:
            self.assertTrue(False)
        # NewUser now exists
        # Test with exact same capitalization
        response = self.client.post(reverse('register'),
            {u'username': [u'NewUser'], u'password1': [u'different'],
            u'email': [u''], u'password2': [u'different']}, follow=True)

        self.assertContains(
            response, "A user with that username already exists")
        self.assertFalse(response.context['user'].is_authenticated())
        self.assertTrue(isinstance(response.context['user'], AnonymousUser))

        # Test with different capitalization
        response = self.client.post(reverse('register'),
            {u'username': [u'newUser'],
            u'password1': [u'different'],
            u'email': [u''], u'password2': [u'different']}, follow=True)

        self.assertContains(
            response, "A user with that username already exists")

        self.assertFalse(response.context['user'].is_authenticated())
        self.assertTrue(isinstance(response.context['user'], AnonymousUser))

    def test_registering_email_that_already_exists(self):
        """
        Registering a new username with the same email is allowed.
        Can reset password by entering username, and email will be sent
        Can reset username by entering email and all usernames will be sent
        """
        self.assertRaises(get_user_model().DoesNotExist,
            get_user_model().objects.get,
            username='NewUser')
        self.client.post(reverse('register'),
            {u'username': [u'NewUser'], u'password1': [u'test'],
                u'email': [u'test@email.com'], u'password2': [u'test']})
        try:
            get_user_model().objects.get(username='NewUser')
        except get_user_model().DoesNotExist:
            self.assertTrue(False)
        # NewUser now exists with email test@email.com
        response = self.client.post(reverse('register'),
            {u'username': [u'DifferentUser'],
            u'password1': [u'different'], u'email': [u'test@email.com'],
            u'password2': [u'different']}, follow=True)

        try:
            user = get_user_model().objects.get(username='DifferentUser')
        except get_user_model().DoesNotExist:
            self.assertTrue(False)

        self.assertEqual(response.context['user'], user)

    def test_login_page_info(self):
        """
        Test that the login page has the correct information
        """
        response = self.client.get(reverse('login'))
        self.assertContains(response, "Login")
        self.assertContains(response, "Username")
        self.assertContains(response, "Password")
        self.assertContains(response, "Forgot Username?")
        self.assertContains(response, "Forgot Password?")

    def test_login(self):
        """
        Test login with valid information works
        Test that username can be incase sensitive
        """
        user = get_user_model().objects.create_user(
            "TestLogin", '', "test")
        response = self.client.post(reverse('login'),
            {u'username': [u'Testlogin'], u'password': [u'test']}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_authenticated())
        self.assertTrue(isinstance(response.context['user'], get_user_model()))
        self.assertFalse(isinstance(response.context['user'], AnonymousUser))

    def test_login_with_invalid_username(self):
        """
        Test login with invalid username displays correct error message
        """
        self.assertRaises(
            get_user_model().DoesNotExist,
            get_user_model().objects.get,
            username='ThisUserDoesNotExist')
        response = self.client.post(reverse('login'),
            {u'username': [u'ThisUserDoesNotExist'], u'password': [u'test']},
            follow=True)
        self.assertContains(response, "Username Does Not Exist.")
        self.assertFalse(response.context['user'].is_authenticated())
        self.assertTrue(isinstance(response.context['user'], AnonymousUser))

    def test_login_with_invalid_password(self):
        """
        Test login with an invalid password displays correct error message
        """
        user = get_user_model().objects.create_user(
            "valid", '', "alsovalid")
        user_id = user.id
        response = self.client.post(reverse('login'),
            {u'username': [u'valid'], u'password': [u'invalid']}, follow=True)
        self.assertTrue(response.status_code, 200)
        self.assertContains(response, "Incorrect password for this username")
        user = get_user_model().objects.get(id=user_id)
        self.assertFalse(response.context['user'].is_authenticated())
        self.assertTrue(isinstance(response.context['user'], AnonymousUser))

    def test_login_with_disabled_account(self):
        user = get_user_model().objects.create_user(
            "disabled", '', "disabled")
        user.is_active = False
        user.save()
        response = self.client.post(reverse('login'),
            {'username': 'disabled', 'password': 'disabled'}, follow=True)
        self.assertContains(response, "Your account has been disabled.")
        self.assertFalse(response.context['user'].is_authenticated())
        self.assertTrue(isinstance(response.context['user'], AnonymousUser))

    def test_redirect_next_with_login(self):
        """
        Test that when you try to access a page
        that you need to be logged in for
        That you will be brought to login page
        And after you login will be
        brought to the original page you were trying to access
        """
        response = self.client.get(reverse('account'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'],
           "http://testserver" + reverse('login') +
           "?next=" + reverse('account'))

        get_user_model().objects.create_user(
            "nexttest", '', "valid")

        # login
        response = self.client.post(reverse('login') +
           "?next=" + reverse('account'),
           {'username': 'nexttest', 'password': 'valid'})
        self.assertTrue(response.status_code, 302)
        self.assertEqual(response['Location'],
           "http://testserver" + reverse('account'))

    def test_logout(self):
        """
        Test logout page works
        """
        user = get_user_model().objects.create_user(
            "logout", '', "logout")
        c = Client()
        c.login(username=user.username, password="logout")
        response = c.get(reverse('dashboard'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_authenticated())
        response = c.get(reverse('logout'))
        self.assertEqual(response.status_code, 302)
        response = c.get(reverse('home'))
        self.assertFalse(response.context['user'].is_authenticated())

    def test_logout_when_not_authenticated(self):
        response = self.client.get(reverse('logout'))
        self.assertTrue(response.status_code, 302)
        self.assertEqual(
            response['Location'], 'http://testserver' + reverse('home'))

    def test_reset_password_information(self):
        """
        Test that the password reset page has the correct information
        """
        response = self.client.get(reverse('reset_password'))
        self.assertContains(response, "Username")
        self.assertContains(response, "Enter your username to send an email to the email associated with the account")
        self.assertContains(response, "Forgot Username?")

    def test_reset_password_with_no_email(self):
        """
        Test that trying to reset your password with an account
        that doesn't have an email will show the appropriate message
        """
        # create user with no email
        get_user_model().objects.create_user(
            "resetemailuser", '', "noemail")
        response = self.client.post(reverse('reset_password'),
            {'username': 'resetEmailUser'}, follow=True)
        self.assertEqual(response.status_code, 200)
        messages = list(response.context['messages'])
        self.assertTrue(len(messages), 1)

        self.assertEqual(
            str(messages[0]), 'There is no email linked with this account.')
        self.assertTrue('There is no email linked with this account.'
            in [str(m) for m in messages])
        self.assertContains(
            response, "There is no email linked with this account")

    def test_reset_password_with_email(self):
        """
        Test reset password with account that has an email
        shows message and sends email
        """
        #TODO: test email is sent
        #create user with email
        get_user_model().objects.create_user(
            "resetemailuser", "test@email.com", "noemail")
        response = self.client.post(reverse('reset_password'),
            {'username': 'resetEmailUser'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Email sent")

    def test_forgot_username_info(self):
        """
        Test forgot username page has the correct information
        """
        response = self.client.get(reverse('forgot_username'))
        self.assertContains(response, "Email")

    def test_forgot_username_with_email(self):
        """
        Test that the Email sent message appears
        """
        user = get_user_model().objects.create_user(
            "resetemailuser", "test@email.com", "noemail")
        response = self.client.post(reverse('forgot_username'),
            {'email': 'test@email.com'}, follow=True)
        self.client.get(reverse('forgot_username'))
        self.assertContains(response, 'Email sent')

    def test_forgot_username_without_email(self):
        """
        Email sent message should be shown otherwise you
        can tell who has an account
        """
        user = get_user_model().objects.create_user(
            "resetemailuser", '', "noemail")
        response = self.client.post(
            reverse('forgot_username'),
            {'email': 'test@email.com'}, follow=True)
        self.assertEqual(response.status_code, 200)

        # Test that there is a flash message
        messages = list(response.context['messages'])
        self.assertTrue(len(messages), 1)
        self.assertEqual(str(messages[0]), 'Email sent.')
        self.assertTrue('Email sent.' in [str(m) for m in messages])
        self.assertContains(response, 'Email sent.')

    def test_change_username(self):
        """
        Test that username works
        """
        user = get_user_model().objects.create_user(
            "firstusername", '', "noemail")
        user_id = user.id
        c = Client()
        c.login(username=user.username, password="noemail")
        response = c.post(reverse('account'),
            {u'username': [u'different'], u'email': [u'']}, follow=True
            )
        self.assertEqual(response.status_code, 200)
        user = get_user_model().objects.get(id=user_id)
        self.assertEqual(user.username, "different")

    def test_change_username_to_a_username_inuse(self):
        """
        Test that changing a username to a username of another user fails
        """
        user1 = get_user_model().objects.create_user(
            "User1", '', "user1pass")
        user2 = get_user_model().objects.create_user(
            "User2", '', "user2pass")
        # Log user1 in
        c = Client()
        c.login(username=user1.username, password="user1pass")
        # Change user1's username to user2's username
        response = c.post(reverse('account'),
            {'username': 'User2'})
        user1 = get_user_model().objects.get(id=user1.id)
        self.assertEqual(user1.username, "User1")

    def test_change_password(self):
        """
        Test change password works
        """
        user = get_user_model().objects.create_user(
            "resetemailuser", "test@email.com", "first")
        user_id = user.id
        c = Client()
        c.login(username=user.username, password="first")
        response = c.post(reverse('account'),
            {u'confirm_new_password': [u'second'],
            u'password': [u'first'],
            u'new_password': [u'second']}, follow=True
            )
        self.assertTrue(response.status_code, 200)
        user = get_user_model().objects.get(id=user_id)
        self.assertTrue(user.check_password('second'))

    def test_change_password_with_invalid_password(self):
        """
        Test that an appropriate message is displayed
        when you enter the current password wrong
        """
        user = get_user_model().objects.create_user(
            "changePassword", '', "initial")
        user_id = user.id
        c = Client()
        c.login(username=user.username, password="initial")
        response = c.post(reverse('account'), {
            'password': 'wrong',
            'new_password': 'new',
            'confirm_new_password': 'new'}, follow=True)
        self.assertTrue(response.status_code, 200)
        self.assertContains(response, "Current Password is Invalid.")

    def test_change_password_with_mismatch_passwords(self):
        """
        Test that an appropriate message is displayed when you enter
        two different new passwords
        """
        user = get_user_model().objects.create_user(
            "changePassword", '', "initial")
        user_id = user.id
        c = Client()
        c.login(username=user.username, password="initial")
        response = c.post(reverse('account'), {
            'password': 'initial',
            'new_password': 'wrong',
            'confirm_new_password': 'right'}, follow=True)
        self.assertTrue(response.status_code, 200)
        messages = list(response.context['messages'])
        self.assertTrue(len(messages), 1)
        self.assertEqual(
            str(messages[0]), "The two new password fields didn't match.")
        self.assertTrue(
            "The two new password fields didn't match."
            in [str(m) for m in messages])
        self.assertFormError(response, 'password_form', 'confirm_new_password',
            "The two new password fields didn't match.")
        #response = c.get(reverse('account'))

        self.assertContains(
            response, "The two new password fields didn't match.")

    def test_change_password_with_invalid_password_and_mismatch(self):
        """
        Test that both error messages appear
        """
        user = get_user_model().objects.create_user(
            "changePassword", '', "initial")
        user_id = user.id
        c = Client()
        c.login(username=user.username, password="initial")
        response = c.post(reverse('account'), {
            'password': 'wrong',
            'new_password': 'wrong',
            'confirm_new_password': 'right'}, follow=True)
        self.assertTrue(response.status_code, 200)
        messages = list(response.context['messages'])
        self.assertTrue(len(messages), 2)
        self.assertEqual(
            str(messages[1]), "Current Password is Invalid.")
        self.assertEqual(
            str(messages[0]), "The two new password fields didn't match.")
        self.assertTrue(
            "Current Password is Invalid." in [str(m) for m in messages])
        self.assertTrue(
            "The two new password fields didn't match."
            in [str(m) for m in messages])
        self.assertFormError(response, 'password_form', 'password',
            "Current Password is Invalid.")
        self.assertFormError(response, 'password_form', 'confirm_new_password',
            "The two new password fields didn't match.")

        self.assertContains(
            response, "The two new password fields didn't match.")
        self.assertContains(
            response, "Current Password is Invalid.")

    def test_change_email(self):
        """
        Test change email works
        """
        user = get_user_model().objects.create_user(
            "resetemailuser", "test@email.com", "first")
        user_id = user.id
        c = Client()
        c.login(username=user.username, password="first")
        response = c.post(reverse('account'),
            {'username': 'resetemailuser', 'email': 'second@email.com'},
            follow=True)
        self.assertTrue(response.status_code, 200)
        user = get_user_model().objects.get(id=user_id)
        self.assertEqual(user.email, "second@email.com")

    def test_update_multiple_user_fields(self):
        """
        Test that you can change multiple user fields at the same time
        """
        user = get_user_model().objects.create_user(
            "resetemailuser", "test@email.com", "first")
        user_id = user.id
        c = Client()
        c.login(username=user.username, password="first")
        response = c.post(reverse('account'),
            {u'username': [u'New'], u'email': [u'second@email.com']},
            follow=True
            )
        self.assertTrue(response.status_code, 200)
        # Check to see the username and email have changed on the page
        self.assertContains(response, 'New')
        self.assertContains(response, 'second@email.com')

        user = get_user_model().objects.get(id=user_id)
        self.assertEqual(user.email, "second@email.com")
        self.assertEqual(user.username, "New")
