from calendar import monthrange
from datetime import datetime
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.utils.safestring import mark_safe
from django.utils import timezone
from django.utils.timezone import utc
from due_dates.models import CourseList, Assignment
from .forms import SelectCourseListForm
from .course_calendar import Calendar, named_month


def select_course_list(request):
    select_course_list_form = SelectCourseListForm()
    if request.method == "POST":
        select_course_list_form = SelectCourseListForm(request.POST)
        if select_course_list_form.is_valid():
            course_list_id = \
                select_course_list_form.cleaned_data['course_list_id']
            today = timezone.now()

            return redirect('calendar', year=today.year,
                month=today.month, course_list_id=course_list_id)

    return render(request, 'course_calendar/select-course-list.html',
        {'select_course_list_form': select_course_list_form})


def calendar(request, year, month, course_list_id=None):
    """
    Show calendar of events for specified courselist
    at the specified year and month
    """

    year = int(year)
    month = int(month)
    from_month = datetime(year, month, 1).utcnow().replace(tzinfo=utc)
    to_month = datetime(
        year, month, monthrange(year, month)[1]).utcnow().replace(tzinfo=utc)

    # Get the list of event this month to pass to the calendar
    if course_list_id is not None:
        # Check if Course List exists
        try:
            valid_course_list = CourseList.objects.get(id=course_list_id)
        except CourseList.DoesNotExist:
            error_title = "Course List Not Found."
            error_message = ''
            return render(request, '_layouts/error.html',
                {'error_title': error_title, 'error_message': error_message})
        events = Assignment.objects.filter(
            course__course_list=valid_course_list).filter(
            due_date__gte=from_month, due_date__lte=to_month)
    elif request.user.is_authenticated():
        events = Assignment.objects.filter(
            course__course_list__user=request.user).filter(
            due_date__gte=from_month, due_date__lte=to_month)
    else:
        # There is no Course List in the URL
        return HttpResponseRedirect(reverse('select_course_list'))

    calendar = Calendar(events, 'due_date').formatmonth(year, month)

    previous_year = year
    previous_month = month - 1
    if previous_month == 0:
        previous_month = 12
        previous_year = year - 1
    next_year = year
    next_month = month + 1
    if next_month == 13:
        next_month = 1
        next_year = year + 1
    year_after = year + 1
    year_before = year - 1
    today = timezone.now()

    return render(request, 'course_calendar/calendar.html', {
        'calendar': mark_safe(calendar),
        'course_list_id': course_list_id,
        'year_today': today.year,
        'month_today': today.month,
        'month': month,
        'month_name': named_month(month),
        'year': year,
        'previous_month': previous_month,
        'previous_month_name': named_month(previous_month),
        'previous_year': previous_year,
        'next_month': next_month,
        'next_month_name': named_month(next_month),
        'next_year': next_year,
        'year_before': year_before,
        'year_after': year_after,
    })


def today(request):
    """
    Show calendar of events this month
    """
    if request.user.is_authenticated():
        today = timezone.now()
        return calendar(request, today.year, today.month)
    else:
        # pick a course list
        return HttpResponseRedirect(reverse('select_course_list'))
