from django import forms
from due_dates.models import CourseList


class SelectCourseListForm(forms.Form):
    course_list_id = forms.CharField(max_length=200)

    def clean_course_list_id(self):
        course_list_id = self.cleaned_data.get("course_list_id")
        #super(SelectCourseListForm, self).validate(course_list_id)

        try:
            course_list = CourseList.objects.get(pk=course_list_id)
        except:
            raise forms.ValidationError("Course List does not exist")

        if course_list.user is not None:
            raise forms.ValidationError(
                "You must login to view this course list calendar")
        else:
            return course_list.id
