from calendar import LocaleHTMLCalendar, SUNDAY, MONDAY
from datetime import date
from itertools import groupby
from django.utils.html import conditional_escape as esc


class Calendar(LocaleHTMLCalendar):
    """
    A calendar is a collection of events which have a time and day
    Adapted from:
    http://drumcoder.co.uk/blog/2010/jun/13/monthly-calendar-django/

    Will work with any model that has a date_field
    just pass the date field when you create the calendar
    For Example:
    events = Assignment.objects.filter(
            course__course_list=valid_course_list).filter(
            due_date__gte=from_month, due_date__lte=to_month)
    calendar = Calendar(events, 'due_date', 'sunday').formatmonth(year, month)
    """
    def __init__(self, events, date_field, starting_day='sunday'):
        super(Calendar, self).__init__()
        self.events = self.group_by_day(events, date_field)

        if starting_day.lower() == 'sunday':
            self.setfirstweekday(SUNDAY)
        elif starting_day.lower() == 'monday':
            self.setfirstweekday(MONDAY)

    def formatday(self, day, weekday):
        if day != 0:
            css_class = self.cssclasses[weekday]
            if date.today() == date(self.year, self.month, day):
                css_class += ' today'
            if day in self.events:
                css_class += ' filled'
                body = []
                for event in self.events[day]:
                    body.append('<a href="%s">' % event.get_absolute_url())
                    body.append(esc(event.name))
                    body.append('</a><br/>')
                return self.day_cell(css_class,
                    '<div class="dayNumber">%d</div> %s'
                        % (day, ''.join(body)))
            return self.day_cell(css_class,
                                 '<div class="dayNumber">%d</div>' % day)
        return self.day_cell('noday', '')

    def formatmonth(self, year, month):
        self.year, self.month = year, month
        return super(Calendar, self).formatmonth(year, month)

    def group_by_day(self, events, date_field):
        field = lambda event: getattr(event, date_field).day
        return dict(
            [(day, list(items))
                for day, items in groupby(events, field)]
        )

    def day_cell(self, css_class, body):
        return '<td class="%s">%s</td>' % (css_class, body)


def named_month(month_number):
    """
    Return the name of the month, given the month number
    """
    return date(1900, month_number, 1).strftime('%B')
