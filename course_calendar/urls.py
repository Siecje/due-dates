from django.conf.urls import patterns, url
from . import views


urlpatterns = patterns('',
    url(r'^select-course-list/$',
        views.select_course_list, name='select_course_list'),
    url(r'^calendar/(?P<year>\d+)/(?P<month>\d+)/(?P<course_list_id>\d+)/$',
        views.calendar, name='calendar'),
    url(r'^calendar/(?P<year>\d+)/(?P<month>\d+)/$',
        views.calendar, name='calendar'),
    url(r'^calendar/$', views.today, name="today"),
)
