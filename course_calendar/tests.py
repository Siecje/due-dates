from datetime import datetime
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.client import Client
from django.contrib.auth import get_user_model
from django.utils import timezone
from due_dates.models import CourseList, Course, Assignment


class CalendarTestSuite(TestCase):

    def test_select_course_list_form_with_no_course_list(self):
        """
        Test that when an anonymous user tries to view the calendar
        they are redirected to select_course_list page
        where they need to enter a course_list id
        Test that if you enter a course_list id that doesn't exist
        you will see appropriate message
        """
        response = self.client.get(reverse('today'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'],
            'http://testserver' + reverse('select_course_list'))
        response = self.client.post(reverse('select_course_list'),
            {u'course_list_id': [u'1']}, follow=True)
        self.assertContains(response, 'Course List does not exist')
        #TODO: test flash messages

    def test_select_course_list_form_with_course_list(self):
        """
        Test that entering a valid course list id will bring you to the
        calendar page
        """
        course_list = CourseList.objects.create()
        response = self.client.get(reverse('today'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'],
            'http://testserver' + reverse('select_course_list'))
        response = self.client.post(reverse('select_course_list'),
            {u'course_list_id': [course_list.id]})
        self.assertEqual(response.status_code, 302)
        lToday = timezone.now()

        self.assertEqual(response['Location'],
            'http://testserver' + reverse('calendar',
                kwargs={'year': lToday.year,
                    'month': lToday.month, 'course_list_id': course_list.id}))
        response = self.client.get(response['Location'])
        self.assertEqual(response.status_code, 200)

    def test_view_private_course_list_in_calendar(self):
        """
        Test that an anonymous user cannot view a private course list
        by entering the correct id in the select_course_list page
        """
        user = get_user_model().objects.create_user('test', '', 'user')
        # make a private course list by adding it to the user
        course_list = CourseList.objects.create(user=user)

        response = self.client.get(reverse('today'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'],
            'http://testserver' + reverse('select_course_list'))
        response = self.client.post(reverse('select_course_list'),
            {u'course_list_id': [course_list.id]}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(
            response, "You must login to view this course list calendar")

    def test_view_calendar_as_authenticated(self):
        user = get_user_model().objects.create_user(
            'brand_new', '', 'unique')
        course_list = CourseList.objects.create(user=user)
        course = Course.objects.create(
            name='New Course', course_list=course_list)
        self.assertEquals(course.assignments.count(), 0)
        assignment = Assignment.objects.create(
            name="Assignment", course=course, due_date=timezone.now())
        assignment_id = assignment.id
        self.assertEquals(course.assignments.count(), 1)
        c = Client()
        c.login(username=user.username, password='unique')
        response = c.get(reverse('today'))
        self.assertEqual(response.status_code, 200)

    def test_view_calender_with_invalid_course_list(self):
        """
        Test that you see an error message when trying to view a course list
        calendar that doesn't exist
        """
        self.assertEqual(CourseList.objects.count(), 0)
        today = timezone.now()
        response = self.client.get(reverse('calendar',
            kwargs={'year': today.year,
                'month': today.month, 'course_list_id': 1}),)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Course List Not Found.")

    def test_view_calendar_as_anonymous_without_course_list(self):
        self.assertEqual(CourseList.objects.count(), 0)
        today = timezone.now()
        response = self.client.get(reverse('calendar',
            kwargs={'year': today.year,
                'month': today.month}),)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'],
            'http://testserver' + reverse('select_course_list'))

    def test_view_calendar_as_authenticated_without_course_list(self):
        user = get_user_model().objects.create_user(
            'brand_new', '', 'unique')
        c = Client()
        c.login(username=user.username, password='unique')

        today = timezone.now()
        response = c.get(reverse('calendar',
            kwargs={'year': today.year,
                'month': today.month}),)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Calendar")

    def test_calendar_year_navigation(self):
        pass

    def test_calendar_month_navigation(self):
        pass
