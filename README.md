TODO List:
---

# Calendar

- Add an Assignment from the calendar page
- Click on a day of the calendar and view due date times and more info
- View just one course in the calendar, with link from course page
- Setting for calendar to start on Sunday

# Accounts
- Pages to Change Username, Email and Password

# Settings

- Setting for term/semester and remove Course List

# General

- Add a Class Schedule/Time Table app
- Make Editable Fields Obvious
- On mouseover of inline fields show image ofW pencil to the right of the field
- One click or double click to edit?
- Course List Drop Down Menu
- Mobile App
- RSS/Atom Feeds
- Add aria attributes

# Course List

- When you visit site, redirect to last Course List viewed, even for anonymous users
- Anyone can add a public course list to their account even if they did not create it
- Add Flash messages
- Can't enter time in datepicker
- Rearrange Courses on the Course List Page
- due date field doesn't look like a datefield
- Should be able to use arrows keys to navigate when editing assignments
- Check the % and make sure total is not above 100% (what about bonus assignments?)
- The ability to add a recurring assignment. For example add a homework assignment due every thursday from start_date to end_date with a total weight of 10%
- link field for assignment to link to source/more info like website etc
- Course Mark does not update when you update a mark using line. You need to refresh the page for it to update.
- keyword for private todolist for sharing or privacy without registering, with password
- Can't enter a float for mark
- Should be able to enter mark out of 100 even if weight is less (like 5)
- Be able to write 2.5/5 for mark and weight as 5%
- https://code.djangoproject.com/ticket/27 ?

# Admin

- Courses in Course Lists


# Benefits to registering

- Course Lists are private
- View more than one Course List in the Calendar
- Don't have to remember Course List ID

# How to Setup a Development Environment
-On Ubuntu
-$ sudo apt-get install postgresql libpq-dev

-$ pip install -r requirements.txt
-$ sudo -u postgres psql postgres
-postgres=# CREATE DATABASE due_dates;
-postgres=# CREATE USER siecje WITH PASSWORD 'siecje';
-CREATE ROLE
-postgres=# GRANT ALL PRIVILEGES ON DATABASE "due_dates" to siecje;
-GRANT
-postgres=# \q
-$ python manage.py syncdb
-$ python manage.py runserver


# How to run tests with coverage
-$ coverage run --source=. manage.py test due_dates
-$ coverage html
-$ firefox htmlcov/index.html
