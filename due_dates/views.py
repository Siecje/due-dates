from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Assignment, Course, CourseList
from .forms import CreateCourseListForm, CourseListNameForm, \
                   CreateCourseForm, CourseNameForm, \
                   AssignmentForm, AssignmentFormSet, DeleteAssignmentForm


def home(request):
    if request.method == 'POST':
        create_course_list_form = CreateCourseListForm(request.POST)
        if create_course_list_form.is_valid():
            new_course_list = create_course_list_form.save(commit=False)
            if request.user.is_authenticated():
                new_course_list.user = request.user

            new_course_list.save()
            return HttpResponseRedirect(
                reverse('course_list', args=(new_course_list.pk,)))
    else:
        create_course_list_form = CreateCourseListForm()
    return render(request, 'due_dates/index.html',
        {'create_course_list_form': create_course_list_form})


@login_required(login_url=reverse_lazy('login'))
def dashboard(request):
    course_lists = request.user.course_lists.all()
    if request.method == 'POST':
        create_course_list_form = CreateCourseListForm(request.POST)
        if create_course_list_form.is_valid():
            new_course_list = create_course_list_form.save(commit=False)
            if request.user.is_authenticated():
                new_course_list.user = request.user

            new_course_list.save()
            return HttpResponseRedirect(
                reverse('course_list', args=(new_course_list.pk,)))
    else:
        create_course_list_form = CreateCourseListForm()

    return render(request, 'due_dates/dashboard.html',
                  {'course_lists': course_lists,
                      'create_course_list_form': create_course_list_form})


def view_course_list(request, course_list_id):
    try:
        course_list = CourseList.objects.get(pk=course_list_id)
    except CourseList.DoesNotExist:
        # Render Error Page
        error_title = "Course List Not Found"
        error_message = ""
        return render(request, '_layouts/error.html',
                      {'error_title': error_title,
                          'error_message': error_message})

    # check if user can view this page
    if course_list.user is not None:
        if course_list.user != request.user:
            # Render Error Page
            error_title = "Course List is Private"
            if not request.user.is_authenticated():
                error_message = "You must login to view this course list"
            else:
                error_message = ""
            return render(request, '_layouts/error.html',
                      {'error_title': error_title,
                          'error_message': error_message})

    course_list_name_form = CourseListNameForm(instance=course_list)
    create_course_form = CreateCourseForm(prefix='course')
    if request.method == 'POST':
        if 'name' in request.POST:
            course_list_name_form = CourseListNameForm(
                request.POST, instance=course_list)
            if course_list_name_form.is_valid():
                course_list_name_form.save()
                return HttpResponseRedirect(
                    reverse('course_list', args=(course_list.pk,)))
        elif 'create_course' in request.POST:
            create_course_form = CreateCourseForm(
                request.POST, prefix='course')
            if create_course_form.is_valid():
                new_course = create_course_form.save(commit=False)
                new_course.course_list = course_list
                new_course.save()
                return HttpResponseRedirect(
                    reverse('course_list', args=(course_list.pk,)))
        elif 'delete' in request.POST:
            course_list.delete()
            if request.user.is_authenticated():
                return HttpResponseRedirect(reverse('dashboard'))
            else:
                return HttpResponseRedirect(reverse('home'))
        elif 'upgrade' in request.POST:
            if course_list.user is None:
                course_list.user = request.user
                course_list.save()
            return HttpResponseRedirect(
                reverse('course_list', args=(course_list.pk,)))

    anonymous = not request.user.is_authenticated()
    user_and_public_list = not anonymous and course_list.user is None
    return render(request, 'due_dates/course_list.html',
        {'course_list': course_list,
         'course_name': course_list.name,
         'course_list_name_form': course_list_name_form,
         'create_course_form': create_course_form,
         'anonymous': anonymous,
         'user_and_public_list': user_and_public_list,
         'domain': request.get_host(),
         'link': request.get_full_path()
        })


def view_course(request, course_list_id, course_id):
    #get_or_error_page
    try:
        course = Course.objects.get(pk=course_id)
    except Course.DoesNotExist:
        # Render Error Page
        error_title = "Course Not Found"
        error_message = ""
        return render(request, '_layouts/error.html',
                      {'error_title': error_title,
                          'error_message': error_message})

    # check if anonymous user can view this page
    if course.course_list.user is not None:
        if course.course_list.user != request.user:
            # Render Error Page
            error_title = "Course is Private"
            #TODO: login link
            if not request.user.is_authenticated():
                error_message = "If this is your course list, " + \
                                "you need to login."
            return render(request, '_layouts/error.html',
                      {'error_title': error_title,
                          'error_message': error_message})

    create_assignment_form = AssignmentForm()
    course_name_form = CourseNameForm(instance=course, prefix='course')
    assignment_formset = AssignmentFormSet(instance=course)
    if request.method == 'POST':
        if 'course-name' in request.POST:
            course_name_form = CourseNameForm(
                request.POST, instance=course, prefix='course')
            if course_name_form.is_valid():
                course_name_form.save()
                return HttpResponseRedirect(
                    reverse('course', args=(course_list_id, course.pk,)))
        elif 'create_assignment' in request.POST:
            create_assignment_form = AssignmentForm(request.POST)
            if create_assignment_form.is_valid():
                new_assignment = create_assignment_form.save(commit=False)
                new_assignment.course = course
                new_assignment.save()
                return HttpResponseRedirect(
                    reverse('course', args=(course_list_id, course_id,)))
        elif 'delete_assignment' in request.POST:
            #TODO: use a form?
            assignment_pk = request.POST['delete_assignment']
            assignment = Assignment.objects.get(pk=assignment_pk)
            assignment.delete()
            return HttpResponseRedirect(
                reverse('course', args=(course_list_id, course_id,)))
        elif 'delete_course' in request.POST:
            #TODO: use a form?
            course.delete()
            return HttpResponseRedirect(
                reverse('course_list', args=(course_list_id,)))
        else:
            assignment_formset = AssignmentFormSet(
                request.POST, request.FILES, instance=course)
            if assignment_formset.is_valid():
                assignment_formset.save()
                return HttpResponseRedirect(
                    reverse('course', args=(course_list_id, course_id,)))

    return render(request, 'due_dates/course.html', {
        'course': course,
        'course_name_form': course_name_form,
        'assignment_formset': assignment_formset,
        'create_assignment_form': create_assignment_form,
        'total_mark': course.calculate_total_mark(),
        'total_weight': course.calculate_total_weight()
    })


def view_assignment(request, course_list_id, course_id, assignment_id):
    try:
        assignment = Assignment.objects.get(pk=assignment_id)
    except Assignment.DoesNotExist:
        # Render Error Page
        error_title = "Assignment Not Found"
        error_message = ""
        return render(request, '_layouts/error.html',
            {'error_title': error_title, 'error_message': error_message})

    # check if anonymous user can view this page
    if assignment.course.course_list.user is not None:
        if assignment.course.course_list.user != request.user:
            # Render Error Page
            error_title = "Assignment is Private"
            error_message = "You must login to view this assignment."
            return render(request, '_layouts/error.html',
                      {'error_title': error_title,
                          'error_message': error_message})

    return render(request, 'due_dates/assignment.html', {
        'assignment': assignment,
    })
