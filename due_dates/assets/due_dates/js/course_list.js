$(document).ready(function(){
  $('#id_course-name').focus();

  $('#toggle_course_list_name_form').mouseenter(function(){
     $('#course_list_name').addClass("editable");
  });

  $('#toggle_course_list_name_form').mouseleave(function(){
     $('#course_list_name').removeClass("editable");
  });

  $('#toggle_course_list_name_form').click(function(){
    /*if ($('#course_list_name_form'))*/
    $(this).toggle();
    $('#course_list_name_form').toggle();
    $('#course_list_name').toggle();
    if ($('#course_list_name_form').is(":visible")) {
      $('#id_name').focus();
      $('#id_name').select();
    }
  });
  $('#hide_course_list_name_form').click(function(){
    $('#toggle_course_list_name_form').click();
    $('#id_name').val($('#course_list_name').text());
  });

  $('#course_list_table').tablesorter();
});
