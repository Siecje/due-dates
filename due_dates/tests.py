from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.client import Client
from django.contrib.auth import get_user_model
from .models import CourseList, Course, Assignment


class DueDatesTestSuite(TestCase):

    def test_form_on_home_page(self):
        """
        Test that the get Started button is on the home page
        """
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('create_course_list_form' in response.context)
        self.assertContains(
            response, "Create a Course List to get Started", status_code=200)

    def test_create_course_list_from_home_page(self):
        """
        Test that you can create a course list from the home page and
        view the course_list after it is created
        """
        course_list_count = CourseList.objects.count()
        response = self.client.post(reverse('home'), {})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(course_list_count + 1, CourseList.objects.count())
        self.assertEqual(response['Location'],
            "http://testserver" + reverse(
                'course_list', args=(CourseList.objects.latest('id').id,)))

    def test_create_course_list_from_home_page_as_authenticated_user(self):
        """
        Test that you can create a course list from the home page as
        an authenticated user and the course_list will be private
        """
        user = get_user_model().objects.create_user(
            'brand_new', '', 'unique')
        self.assertNotEqual(user.id, None)
        self.assertEqual(user.course_lists.count(), 0)
        c = Client()
        c.login(username=user.username, password='unique')
        response = c.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_authenticated())
        response = c.post(reverse('home'), {}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_authenticated())
        self.assertEqual(response.context['user'].id, user.id)
        user = get_user_model().objects.get(id=user.id)
        self.assertEqual(user.course_lists.count(), 1)

    def test_dashboard_create_course_list(self):
        """
        Test that creating a course_list from the dashboard page works
        """
        user = get_user_model().objects.create_user(
            'dashboard_user', '', 'unique')
        self.assertEqual(user.course_lists.count(), 0)
        c = Client()
        c.login(username=user.username, password='unique')
        response = c.post(reverse('dashboard'), {}, follow=True)
        self.assertEqual(response.status_code, 200)
        user = get_user_model().objects.get(id=user.id)
        self.assertEqual(user.course_lists.count(), 1)

    def test_course_list_message_with_anonymous(self):
        """
        Test that a message is displayed about it being a public course list
        for anonymous users
        """
        course_list = CourseList.objects.create()
        response = self.client.get(
            reverse('course_list', args=(course_list.id,)))
        self.assertContains(response, "public", status_code=200)

    def test_course_list_message_with_authenticated(self):
        """
        Test public message with authenticated user with public course list
        """
        course_list = CourseList.objects.create()
        user = get_user_model().objects.create_user('john', '', 'password')
        c = Client()
        c.login(username=user.username, password='password')
        response = c.get(reverse('course_list', args=(course_list.id,)))
        self.assertContains(response, "public", status_code=200)

    def test_no_course_list_message_with_authenticated(self):
        """
        Test no message with private course list
        that belongs to an authenticated user
        """
        course_list = CourseList.objects.create()
        user = get_user_model().objects.create_user('john', '', 'password')
        course_list.user = user
        course_list.save()
        response = self.client.get(
            reverse('course_list', args=(course_list.id,)))
        self.assertNotContains(response, "public", status_code=200)

    def test_private_message_with_authenticated_user(self):
        """
        Test private message with private course_list and authenticated user
        """
        course_list_user = get_user_model().objects.create_user(
            'course_user', '', 'course_password')
        course_list = CourseList.objects.create(user=course_list_user)

        new_user = get_user_model().objects.create_user('new', '', 'password')

        c = Client()
        c.login(username=new_user.username, password='password')
        response = c.get(reverse('course_list', args=(course_list.id,)))
        self.assertContains(
            response, "Course List is Private", status_code=200)

    def test_private_message_with_private_course_list_and_anonymous_user(self):
        """
        Test private error message is shown
        with private course_list and anonymous user
        """
        course_list = CourseList.objects.create()
        new_user = get_user_model().objects.create_user('new', '', 'password')
        course_list.user = new_user
        course_list.save()
        response = self.client.get(
            reverse('course_list', args=(course_list.id,)))
        self.assertContains(
            response, "Course List is Private", status_code=200)
        self.assertContains(
            response, "login to view this course list", status_code=200)

    def test_course_list_name_change(self):
        """
        Test that changing the name of the course list works
        """
        course_list = CourseList.objects.create()
        self.assertEquals(course_list.name, 'Term 1')
        response = self.client.post(
            reverse('course_list', args=(course_list.id,)))
        self.assertNotContains(response, 'New', status_code=200)
        response = self.client.post(
            reverse('course_list', args=(course_list.id,)),
            {u'name': [u'New']})
        response = self.client.post(
            reverse('course_list', args=(course_list.id,)))
        self.assertContains(response, 'New', status_code=200)
        course_list = CourseList.objects.get(id=course_list.id)
        self.assertEquals(course_list.name, 'New')

    def test_view_course_does_not_exist(self):
        """
        Test that viewing a course list that doesn't exist will display
        an appropriate message
        """
        self.assertEqual(Course.objects.count(), 0)
        response = self.client.get(reverse('course', args=(1, 1,)))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Course Not Found")

    def test_add_course_list_to_account(self):
        """
        Test that an authenticated user can add a course list to his or her
        account to make the course list private
        """
        course_list = CourseList.objects.create()
        new_user = get_user_model().objects.create_user('new', '', 'password')
        c = Client()
        c.login(username=new_user.username, password="password")
        response = c.post(reverse('course_list', args=(course_list.id,)),
            {'upgrade': ''})
        course_list = CourseList.objects.get(id=course_list.id)
        self.assertEqual(course_list.user, new_user)

    def test_course_creation(self):
        """
        Test that creating a course on the course list page
        the page now contains the text of the course name
        """
        course_list = CourseList.objects.create()
        course_list_id = course_list.id
        self.assertEqual(course_list.courses.count(), 0)
        response = self.client.get(
            reverse('course_list', args=(course_list_id,)))
        self.assertNotContains(response, 'New Course', status_code=200)
        response = self.client.post(
            reverse('course_list', args=(course_list_id,)),
            {u'create_course': [u'Add'], u'course-name': [u'New Course']})
        response = self.client.get(
            reverse('course_list', args=(course_list_id,)))
        self.assertContains(response, 'New Course', status_code=200)
        course_list = CourseList.objects.get(id=course_list_id)
        self.assertEqual(course_list.courses.count(), 1)

    def test_course_creation_on_private_course_list(self):
        """
        Test posting to a private course_list page
        to ensure a course is not created
        """
        course_list = CourseList.objects.create()
        new_user = get_user_model().objects.create_user('new', '', 'password')
        course_list.user = new_user
        course_list.save()
        self.assertEqual(course_list.courses.count(), 0)
        response = self.client.post(
            reverse('course_list', args=(course_list.id,)),
            {u'create_course': [u'Add'], u'course-name': [u'New Course']})
        self.assertEqual(response.status_code, 200)
        course_list = CourseList.objects.get(id=course_list.id)
        self.assertEqual(course_list.courses.count(), 0)

    def test_course_list_delete(self):
        """
        Test that after deleting the course list
        the URL will 404
        """
        course_list = CourseList.objects.create()
        course_list_id = course_list.id
        response = self.client.post(
            reverse('course_list', args=(course_list.id,)),
            {u'delete': [u'Delete New']})
        self.assertRaises(
            CourseList.DoesNotExist, CourseList.objects.get, id=course_list_id)

    def test_course_list_delete_as_authenticated(self):
        """
        Test that after deleting the course list
        authenticated users will be redirect to the 'dashboard' page
        and the old URL will 404
        """
        new_user = get_user_model().objects.create_user('new', '', 'password')
        course_list = CourseList.objects.create(user=new_user)
        course_list_id = course_list.id
        c = Client()
        c.login(username=new_user.username, password='password')
        response = c.post(
            reverse('course_list', args=(course_list.id,)),
            {u'delete': [u'Delete New']})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response['Location'], 'http://testserver' + reverse('dashboard'))
        self.assertRaises(
            CourseList.DoesNotExist, CourseList.objects.get, id=course_list_id)

    def test_view_course_list_does_not_exist(self):
        """
        Test that viewing a course list that doesn't exist will display
        an appropriate message
        """
        self.assertEqual(CourseList.objects.count(), 0)
        response = self.client.get(reverse('course_list', args=(1,)))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Course List Not Found")

    def test_course_name_change(self):
        """
        Test that changing course name works
        """
        course_list = CourseList.objects.create()
        new_course = Course.objects.create(
            name='New Course', course_list=course_list)
        response = self.client.get(
            reverse('course', args=(course_list.id, new_course.id,)))
        self.assertNotContains(response, 'Updated')
        response = self.client.post(
            reverse('course', args=(course_list.id, new_course.id,)),
            {u'course-name': [u'Updated']})
        new_course = Course.objects.get(id=new_course.id)
        self.assertEqual(new_course.name, 'Updated')

    def test_course_name_change_by_anonymous_on_private_course(self):
        """
        Test that you can't change the course name of a private list as
        an anonymous user
        """
        course_list = CourseList.objects.create()
        new_course = Course.objects.create(
            name='New Course', course_list=course_list)
        new_user = get_user_model().objects.create_user('new', '', 'password')
        course_list.user = new_user
        course_list.save()
        response = self.client.get(
            reverse('course', args=(course_list.id, new_course.id,)))
        self.assertNotContains(response, "Updated", status_code=200)
        response = self.client.post(
            reverse('course', args=(course_list.id, new_course.id,)),
            {u'course-name': [u'Updated']})
        self.assertNotContains(response, "Updated")
        new_course = Course.objects.get(id=new_course.id)
        self.assertEqual(new_course.name, 'New Course')

    def test_course_name_change_by_different_auth_user(self):
        """
        Test that an authenticated user can't change the name
        of another user's private course
        """
        course_list = CourseList.objects.create()
        new_course = Course.objects.create(
            name='New Course', course_list=course_list)
        user1 = get_user_model().objects.create_user('user1', '', 'password')
        course_list.user = user1
        course_list.save()
        user2 = get_user_model().objects.create_user('user2', '', 'password')
        c = Client()
        c.login(username=user1.username, password=user2.password)
        response = c.post(
            reverse('course', args=(course_list.id, new_course.id,)),
            {u'course-name': [u'Updated']})
        new_course = Course.objects.get(id=new_course.id)
        self.assertEqual(new_course.name, 'New Course')

    def test_delete_course(self):
        course_list = CourseList.objects.create()
        course = Course.objects.create(
            name='New Course', course_list=course_list)
        course_id = course.id
        response = self.client.post(
            reverse('course', args=(course_list.id, course.id,)),
            {u'delete_course': [u'Delete Course']})
        self.assertRaises(
            Course.DoesNotExist, Course.objects.get, id=course_id)

    def test_course_create_assignment(self):
        """
        Test that you can create an assignment
        and it will appear on the page
        Also test that you don't need a due date
        """
        course_list = CourseList.objects.create()
        course = Course.objects.create(
            name='New Course', course_list=course_list)
        self.assertEquals(course.assignments.count(), 0)
        response = self.client.get(
            reverse('course', args=(course_list.id, course.id,)))
        self.assertNotContains(response, 'Test', status_code=200)
        response = self.client.post(
            reverse('course', args=(course_list.id, course.id,)),
            {u'due_date': [u''], u'name': [u'Test'], u'weight': [u'0'],
            u'mark': [u'0'], u'create_assignment': [u'Add Assignment']})
        response = self.client.get(
            reverse('course', args=(course_list.id, course.id,)))
        self.assertContains(response, 'Test')
        course = Course.objects.get(id=course.id)
        self.assertEquals(course.assignments.count(), 1)

    def test_course_edit_assignment(self):
        """
        Test that editing an assignment works
        Changing the name should have the name on the page on reload
        """
        course_list = CourseList.objects.create()
        course = Course.objects.create(
            name='New Course', course_list=course_list)
        self.assertEquals(course.assignments.count(), 0)
        assignment = Assignment.objects.create(
            name="Assignment", course=course)
        assignment_id = assignment.id
        self.assertEquals(course.assignments.count(), 1)
        response = self.client.get(
            reverse('course', args=(course_list.id, course.id,)))
        self.assertNotContains(response, 'Math Test')
        response = self.client.post(
            reverse('course', args=(course_list.id, course.id,)),
            {u'assignments-TOTAL_FORMS': [u'1'],
             u'assignments-0-name': [u'Math Test'],
             u'assignments-0-id': [u'%d' % assignment_id],
             u'assignments-0-weight': [u'0.0'],
             u'assignments-0-mark': [u'0.0'],
             u'assignments-0-due_date': [u''],
             u'assignments-INITIAL_FORMS': [u'1'],
             u'assignments-MAX_NUM_FORMS': [u'1000']}
            )
        response = self.client.get(
            reverse('course', args=(course_list.id, course.id,)))
        self.assertContains(response, 'Math Test')
        assignment = Assignment.objects.get(id=assignment_id)
        self.assertEqual(assignment.name, 'Math Test')

    def test_delete_assignment(self):
        course_list = CourseList.objects.create()
        course = Course.objects.create(
            name='New Course', course_list=course_list)
        self.assertEquals(course.assignments.count(), 0)
        assignment = Assignment.objects.create(
            name="Assignment", course=course)
        assignment_id = assignment.id
        self.assertEquals(course.assignments.count(), 1)
        response = self.client.post(
            reverse('course', args=(course_list.id, course.id,)),
            {'delete_assignment': assignment_id})
        self.assertRaises(
            Assignment.DoesNotExist, Assignment.objects.get, id=assignment_id)

    def test_view_assignment(self):
        course_list = CourseList.objects.create()
        course = Course.objects.create(
            name='New Course', course_list=course_list)
        self.assertEquals(course.assignments.count(), 0)
        assignment = Assignment.objects.create(
            name="Assignment", course=course)
        assignment_id = assignment.id
        self.assertEquals(course.assignments.count(), 1)
        response = self.client.get(assignment.get_absolute_url())
        self.assertEqual(response.status_code, 200)

    def test_view_assignment_that_does_not_exist(self):
        response = self.client.get(
            reverse('assignment',
                args=(1, 1, 1,)))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Assignment Not Found")

    def test_view_private_assignment(self):
        user = get_user_model().objects.create_user(
            'brand_new', '', 'unique')
        course_list = CourseList.objects.create(user=user)
        course = Course.objects.create(
            name='New Course', course_list=course_list)
        self.assertEquals(course.assignments.count(), 0)
        assignment = Assignment.objects.create(
            name="Assignment", course=course)
        assignment_id = assignment.id
        self.assertEquals(course.assignments.count(), 1)
        response = self.client.get(
            reverse('assignment',
                args=(course_list.id, course.id, assignment_id,)))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Assignment is Private")
        self.assertContains(
            response, "You must login to view this assignment.")
