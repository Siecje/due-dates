from django.db import models
from django.db.models.aggregates import Sum
from django.conf import settings
from django.core.urlresolvers import reverse


class Assignment(models.Model):
    name = models.CharField(max_length=200)
    due_date = models.DateTimeField(null=True, blank=True)
    mark = models.FloatField(default=0, blank=True)
    weight = models.FloatField(default=0, blank=True)

    course = models.ForeignKey('Course', related_name='assignments')

    class Meta:
        ordering = ['due_date']

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('assignment',
            args=(self.course.course_list.id, self.course.id, self.id,))


class Course(models.Model):
    name = models.CharField(max_length=200)

    course_list = models.ForeignKey('CourseList', related_name='courses')

    class Meta:
        ordering = ["id", "name"]

    def calculate_total_mark(self):
        """
        Calculate the mark from all of the tasks in the Task List
        """
        total_mark = 0
        for assignment in self.assignments.all():
            #TODO: how to be able to write 2.5/5 for mark and weight as 5%
            total_mark += (assignment.mark / float(100)) * assignment.weight

        return total_mark

    def calculate_total_weight(self):
        return Assignment.objects.filter(
            course=self.id).aggregate(Sum('weight'))['weight__sum']

    def __unicode__(self):
        return self.name


class CourseList(models.Model):
    #TODO: uuid because of public course lists?
    name = models.CharField(max_length=200, default="Term 1")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,
        related_name='course_lists')

    def get_absolute_url(self):
        return reverse('course_list', args=(self.pk,))

    def __unicode__(self):
        return self.name
