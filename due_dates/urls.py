from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

from . import views
from accounts import urls as account_urls
from course_calendar import urls as calendar_urls


urlpatterns = patterns('',
    # home
    url(r'^$', views.home, name='home'),

    # accounts
    url(r'^', include(account_urls)),

    # calendar
    url(r'^', include(calendar_urls)),

    # Admin
    url(r'^admin/', include(admin.site.urls)),

    # course
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^(?P<course_list_id>\d+)/$', views.view_course_list,
        name='course_list'),
    url(r'^(?P<course_list_id>\d+)/course/(?P<course_id>\d+)/$',
        views.view_course, name='course'),
    url(r'^(?P<course_list_id>\d+)/course/(?P<course_id>\d+)/assignment/(?P<assignment_id>\d+)/$',
        views.view_assignment, name='assignment'),
)
