from .base import *

SECRET_KEY = os.environ.get('SECRET_KEY')

# Heroku settings
import dj_database_url

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['due-dates.herokuapp.com']

DATABASES = {'default': dj_database_url.config()}
