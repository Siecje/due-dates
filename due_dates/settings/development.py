from .base import *

DEBUG = True

ALLOWED_HOSTS = ['*']
SECRET_KEY = 'development'

DATABASES = {
    'default': {
        # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        # Or path to database file if using sqlite3.
        'NAME': 'due_dates',
        # The following settings are not used with sqlite3:
        'USER': 'siecje',
        'PASSWORD': 'siecje',
        # Empty for localhost through domain sockets
        # or '127.0.0.1' for localhost through TCP.
        'HOST': '127.0.0.1',
        # Set to empty string for default.
        'PORT': '',
    }
}

#Email settings
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = 'testing@example.com'
