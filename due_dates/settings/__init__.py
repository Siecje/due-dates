#load production.py if it exists
#production.py loads base.py and adds public only settings
try:
    from .production import *
except ImportError:
    from .base import *
