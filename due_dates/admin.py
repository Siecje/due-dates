from django.contrib import admin
from .models import Assignment, Course, CourseList

class CourseListAdmin(admin.ModelAdmin):
	list_display = ('name', 'user')

admin.site.register(Assignment)
admin.site.register(Course)
admin.site.register(CourseList, CourseListAdmin)
