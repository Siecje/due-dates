from django import forms
from django.forms.models import inlineformset_factory
from bootstrap3_datetime.widgets import DateTimePicker
from .models import Assignment, Course, CourseList


class AssignmentForm(forms.ModelForm):
    error_css_class = 'error'
    required_css_class = 'required'

    class Meta:
        model = Assignment
        fields = ['name', 'due_date', 'mark', 'weight']
        labels = {'weight': 'Weight (%)'}
        help_texts = {'weight': 'in percent'}
        #TODO: if I use this I can't edit assignments
        widgets = {
            'due_date': DateTimePicker(
                options={
                    "format": "YYYY-MM-DD hh:mm",
                    "pickTime": True,
                    "language": "en",
                    "minuteStepping": 5
                    }
                ),
            }
        #widgets = {
            #'due_date': forms.TextInput(
                #attrs={
                    #'class': 'datepicker',
                    #'data-date-format': 'yyyy-mm-dd',
                    #'placeholder': 'YYYY-MM-DD'
                #}
            #),}


AssignmentFormSet = inlineformset_factory(Course, Assignment,
    form=AssignmentForm, extra=0, can_delete=True, can_order=True)


class CreateCourseForm(forms.ModelForm):
    error_css_class = 'error'
    required_css_class = 'required'

    class Meta:
        model = Course
        fields = ['name']
        labels = {
            'name': 'Course Name',
        }
        widgets = {'name': forms.TextInput(attrs={'size': 40})}


class CreateCourseListForm(forms.ModelForm):
    class Meta:
        model = CourseList
        fields = []


class CourseListNameForm(forms.ModelForm):
    error_css_class = 'error'
    required_css_class = 'required'

    class Meta:
        model = CourseList
        fields = ['name']


class CourseNameForm(forms.ModelForm):
    error_css_class = 'error'
    required_css_class = 'required'

    class Meta:
        model = Course
        fields = ['name']


class DeleteAssignmentForm(forms.ModelForm):
    class Meta:
        model = Assignment
        fields = ['id']
        widgets = {
            'id': forms.HiddenInput(),
        }
