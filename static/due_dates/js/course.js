$(document).ready(function(){
  $('#id_name').focus();

  /*$('.datepicker').datepicker();*/

  $('#toggle_course_name_form').mouseenter(function(){
     $('#course_name').addClass("editable");
  });

  $('#toggle_course_name_form').mouseleave(function(){
     $('#course_name').removeClass("editable");
  });

  $('#toggle_course_name_form').click(function(){
    $(this).toggle();
    $('#course_name_form').toggle();
    $('#course_name').toggle();
    if ($('#course_name_form').is(":visible")) {
      $('#id_course-name').focus();
      $('#id_course-name').select();
    }
  });
  $('#hide_course_name_form').click(function(){
    $('#toggle_course_name_form').click();
    $('#id_course-name').val($('#course_name').text());
  });

  /* Assignments Formset */
  $('#assignment_table input').change(function() {
    $('#save_assignments_form').toggle();
    $('#cancel_assignments_form').toggle();
    $('#asssignment_table_footer').toggle();
  });
  $('#save_assignments_form').click(function(){
    $('#assignments_form').submit();
  });
  $('#assignment_table').tablesorter({headers:{4:{ sorter:false}}});
});
